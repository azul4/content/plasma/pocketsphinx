# pocketsphinx

Required by subtitlecomposer. Lightweight speech recognition engine

http://cmusphinx.sourceforge.net

https://github.com/cmusphinx/pocketsphinx/

<br><br>
How to clone this repository:
```
git clone https://gitlab.com/azul4/content/plasma/pocketsphinx.git
```
